[ "$(stat -c %U /data/db)" = mongodb ] || chown -R mongodb /data/db
mongod --bind_ip_all &
rabbitmqctl status | grep rabbit
mongo --version
apk add imagemagick imagemagick-dev
docker build -t registry.gitlab.com/richar-osse/cenit:rb-255 .
docker push registry.gitlab.com/richar-osse/cenit:rb-255
